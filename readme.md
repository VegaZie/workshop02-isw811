# Workshop02 - Implementación LAMP en Bookworm

## Cambio del hostname de la máquina Bookworm

Primeramente accedemos a la máquina Bookworm mediante el comando `vagrant ssh`

Una vez que nos conectemos a la máquina virtual se debe ejecutar el siguiente comando:

```bash
sudo hostnamectl set-hostname webserver
```

Para continuar con el cambio del nombre del host, vamos a editarlo con `nano` ejecutando el siguiente comando `nano /etc/hosts`

![Cambio del hostname a webserver](images/hostnameC.PNG)

Una vez cambiamos el nombre a webserver guardamos con `ctrl + o` y salimos con `ctrl + x`

Seguidamente de esto salimos de la máquina virtual y volvemos a acceder.

```bash
exit
vagrant ssh
```

## Instalar paquetes Debian

Primeramente antes de instalar los paquetes de Debian devemos actualizar la lista de los paquetes disponibles.

```bash
sudo apt-get update
```

Seguidamente instalamos Apache2, PHP, MySQL y otros útiles.

```bash
sudo apt-get install vim vim-nox curl apache2 mariadb-server mariadb-client php8.2 php8.2-bcmath php8.2-curl php8.2-mbstring php8.2-mysql php8.2-xml php8.2-mcrypt
```

## Configuración de entrada de hostfile

El primer paso es comprobar que podemos ver la máquina virtual a través de la IP de su red de *Host Olny Network.*

```bash
ping 192.168.37.10
```

Debido a que mi computadora usa un SO de tipo Windows (No soy un crack), debemos abrir la terminal *Símbolo del sistema* con permisos de administrador (privilegios elevados); una vez hecho esto la terminal se abrirá en ubicación `c:\windows\system32`, seguidamente debemos llegar a la siguiente ruta: `c:\windows\system32\drivers\etc` para esto se ejecutan los siguientes comandos:

```bash
cd drivers
cd etc
notepad hosts
```

Una vez se abrio el archivo `hosts` en el editor de texto, agregamos una entrada a nuestro dominio `vegazie.isw811.xyz` hacia el servidor con IP `192.168.37.10`, esto de la siguiente manera:

```bash
192.168.37.10 vegazie.isw811.xyz
```

Luego de editar el archivo `hosts` debemos verificar si el nombre del dominio ingreasado:

```bash
ping vegazie.isw811.xyz
```

Luego para verificar que logramos llegar al *<< defaultsite >>* de Apache2, esto desde un navegador en la máquina anfitriona (utilizando el navegador en modo incognito de preferencia), para esto visitamos la URL: [vegazie.isw811.xyz](vegazie.isw811.xyz)

## Preparar el servidor para hospedar varios sitios

Para utilizar Apache como un proxy reverso, para hospedar varios sitios web en la misma IP y en el mismo puerto, vamos a necesitar instalar el módulo `vhost_alias`. Podemos aprovechar e instalar `rewrite` que es un módulo requerido *ssl* que nos permitirá publicar los sitios por medio del protocolo https.

```bash
sudo a2enmod vhost_alias rewrite ssl
sudo systemctl restart apache2
```

## Configurar la sicronización en 2 vías de Vagrant

Desde la máquina afitriona vamos a editar el *Vagrantfile* para agregar la directiva que se encargará de mapear el folder `./sites` hacia la ruta `/home/vagrant/sites`, definiendo tanto para el propietario, como para el grupo, el valor `wwww-data`, que representa el usuario apache.

![Apache como 2 vías](images/vargrant2vias.PNG)

Ahora creamos el folder `sites`, estando ubicados a nivel de la carpeta `webserver` (en la máquina anfitriona), luego apagamos la máquina virtual, la reiniciamos y nos conectamos.

```bash
mkdir sites
vagrant halt
vagrant up
vagrant ssh
```

Al conectarnos en la máquina virtual podemos comprobar que existe la carpeta `sites` en el *home* del usuario *vagrant*, de la siguiente manera.

```bash
pwd ls -l
```

![Carpeta sites](images/sites.PNG)

Nótese que la carpeta `sites` se encuentra motanda en dos ubicaciones. En `/vagrant/sites`, donde le pertenece al usuario y el grupo; y en la ruta `/home/vagrant/sites`, donde le pertence al usuario y grupo www-data, que corresponde al usuario y grupo de Apache2.

![Carpeta en dos ubicaciones](images/carpetaCompartida.PNG)

## Configurar nuestro primer vhost

Desde la máquina anfitriona, a nivel de la carpeta `webserver` vamos a ejecutar los siguientes comandos.

```bash
mkdir confs
cd confs
touch vegazie.isw811.xyz.conf
code vegazie.isw811.xyz.conf
```

Desde el editor VSCode vamos a agregar el siguiente contenido al archivo `vegazie.isw811.xyz.conf` 

```bash
<VirtualHost *:80>
ServerAdmin webmaster@vegazie.isw811.xyz
ServerName vegazie.isw811.xyz

DirectoryIndex index.php index.html
  DocumentRoot /home/vagrant/sites/vegazie.isw811.xyz

  <Directory /home/vagrant/sites/vegazie.isw811.xyz>
  DirectoryIndex index.php index.html
  AllowOverride All
  Require all granted
  </Directory>
  
    ErrorLog ${APACHE_LOG_DIR}/vegazie.isw811.xyz.error.log
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/vegazie.isw811.xyz.access.log combined
 </VirtualHost>
```

Luego vamos a la máquina virtual y nos movemos a la ruta `/vagrant/confs`

```bash
pwd
cd /vagrant
cd confs
ls -l
```

Como se muestra en la siguiente imagen, en la ruta `/vagrant/confs` de la máquina virtual, podemos visualizar el archivo `vegazie.isw811.xyz.conf`

![Archivo conf](images/archivoConf.PNG)

Ahora copiamos el archivo desde `/vagrant/confs` a `/etc/apache2/sites-available/`, con el siguiente comando. 

```bash
sudo cp vegazie.isw811.xyz.conf /etc/apache2/sites-available/
```

Ahora habilitamos el nuevo sitio.

```bash
sudo a2ensite vegazie.isw811.xyz.conf
```

Luego hay que verificar que no existan errores, antes de reiniciar Apache2.

```bash
sudo apache2ctl - t
```

El único error que se muestra, es uno relacionado con la ausencia de la directiva `Servername` en el archivo  `/etc/apache2/apache2.conf`

Para agregar la directiva al archivo `/etc/apache2/apache2.conf`, podemos utilizar el siguiente comando. 

```bash
echo "ServerName webserver" | sudo tee -a /etc/apache2/apache2.conf
```

Para comprobar que se agregó correctamente, ejecutamos el siguiente comando.

```bash 
cat /etc/apache2/apache2.conf | grep ServerName
```

Ahora al lanzar el comando `sudo apache2ctl -t`, vemos que aparece un error relacionado con la existencia del folder del sitio `/home/vagrant/sites` 

Para corregirlo vamos a crear un sitio web de ejemplo. Desde la máquina afintriona ejecutamos los siguientes comandos, estan ubicados en el folder `webserver`.

```bash
cd sites
mkdir vegazie.isw811.xyz
cd vegazie.isw811.xyz
touch index.html
mkdir images
code index.html
```

Descargamos la imagen de un gato del siguiente link: [https://isw811.s3.amazonaws.com/images/gatito.png](https://isw811.s3.amazonaws.com/images/gatito.png)

La guardamos en la siguiente ruta: `~/home/david/ISW811/VMs/webserver/sites/vegazie.isw811.xyz/images`

![Gatito](images/gatito.png)

El contenido de mi *index.html* de prueba es el siguiente:

```html
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de prueba del curso Aplicaciones Web Utilizando Software Libre</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
            text-align: center;
        }

        header {
            background-color: #007bff;
            color: #fff;
            padding: 20px;
        }

        h1 {
            font-size: 24px;
        }

        p {
            font-size: 18px;
            margin: 20px;
        }

        img {
            max-width: 100%;
            height: auto;
        }
    </style>
</head>
<body>
    <header>
        <h1>Página de prueba del curso Aplicaciones Web Utilizando Software Libre</h1>
    </header>
    <main>
        <p>¿Sabías que el software libre es como un buffet libre para tu computadora? ¡Puedes servirte a gusto sin preocuparte por la cuenta! Únete a la revolución del software libre y saborea la libertad de elegir, modificar y compartir.</p>
        <img src="images/gatito.png" alt="Gatito">
    </main>
</body>
</html>
```

Volvemos a comprobar la sintaxis hasta obtener un *<< Syntax Ok >>*

![Syntax Ok](images/syntaxok.PNG)

Finalmente reiniciamos apache en la máquina virtual y comprobamos el sitio desde la máquina anfitriona.

```bash
sudo systemctl restart apache2
```

Finalmente desde la máquina afitriona (de preferencia en un navegador en modo incognito), visitamos la URL [http://vegazie.isw811.xyz/](http://vegazie.isw811.xyz/) El sitio debería de verse de la siguiente manera

![Index.html](images/indexhtml.PNG)
